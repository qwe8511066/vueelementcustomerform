import request from '@/utils/request'

// 我的考勤
// 我的考勤统计
export const getMyAttendanceTotal = postData => {
  return request({
    url: '/api/services/app/Attendance/GetAttendanceMonthStatisticalSingle',
    method: 'post',
    data: { searchYearMonth: postData }
  })
}

// 获取假期管理
export const getLeaveManage = () => {
  return request({
    url: '/api/services/app/Attendance/GetHolidayManageList',
    method: 'post'
  })
}

// 我的考勤日程
export const getMyAttendance = postData => {
  return request({
    url: '/api/services/app/Attendance/GetAttendanceDayStatisticalList',
    method: 'post',
    data: { SearchYearMonth: postData }
  })
}

// 删除请假记录
export const submitDeleteLeaveRecord = postData => {
  return request({
    url: '/api/services/app/Attendance/DeleteEmployeeHolidayRecordIds',
    method: 'post',
    data: { ids: postData }
  })
}

// 删除加班记录
export const submitDeleteOvertimeRecord = postData => {
  return request({
    url: '/api/services/app/Attendance/DeleteEmployeeWorkOvertimeRecordsIds',
    method: 'post',
    data: { ids: postData }
  })
}
//==============================================================================================

// 考勤设置
// 提交创建排班表
export const submitNewScheduling = postData => {
  return request({
    url: '/api/services/app/Attendance/CreateAttendanceSchedule',
    method: 'post',
    data: { attendanceSchedule: postData }
  })
}

// 获取排班表详情
export const getSchedulingDetail = postData => {
  return request({
    url: '/api/services/app/Attendance/GetAttendanceScheduleDetail',
    method: 'post',
    data: { id: postData }
  })
}

// 提交更新排班表
export const submitEditScheduling = postData => {
  return request({
    url: '/api/services/app/Attendance/UpdateAttendanceSchedule',
    method: 'post',
    data: postData
  })
}

// 提交创建考勤组
export const submitNewGroup = postData => {
  return request({
    url: '/api/services/app/Attendance/CreateAttendanceGroupManagement',
    method: 'post',
    data: { attendanceGroupManagement: postData }
  })
}

// 提交重编辑考勤组
export const submitEditGroup = postData => {
  return request({
    url: '/api/services/app/Attendance/UpdateAttendanceGroupManagement',
    method: 'post',
    data: postData
  })
}

// 获取考勤组详情
export const getGroupDetail = postData => {
  return request({
    url: '/api/services/app/Attendance/GetAttendanceGroupManagementDetail',
    method: 'post',
    data: { id: postData }
  })
}

// 考勤组删除
export const submitDeleteGroup = postData => {
  return request({
    url: '/api/services/app/Attendance/DeleteAttendanceGroupManagementIds',
    method: 'post',
    data: { ids: postData }
  })
}


// 提交创建假期
export const submitNewVacation = postData => {
  return request({
    url: '/api/services/app/Attendance/CreateHolidayManage',
    method: 'post',
    data: { holidayManage: postData }
  })
}

// 提交重编辑假期
export const submitEditVacation = postData => {
  return request({
    url: '/api/services/app/Attendance/UpdateHolidayManage',
    method: 'post',
    data: postData
  })
}

// 获取详情
export const getVacationDetail = postData => {
  return request({
    url: '/api/services/app/Attendance/GetHolidayManageDetail',
    method: 'post',
    data: { id: postData }
  })
}

// 假期删除
export const submitDeleteVacation = postData => {
  return request({
    url: '/api/services/app/Attendance/DeleteHolidayManageByIds',
    method: 'post',
    data: { ids: postData }
  })
}

//==============================================================================================================================

// 班次管理
// 提交添加班次
export const submitNewShift = postData => {
  return request({
    url: '/api/services/app/Attendance/CreateShiftManagement',
    method: 'post',
    data: { shiftManagement: postData }
  })
}

// 提交重编辑班次
export const submitChangeShift = postData => {
  return request({
    url: '/api/services/app/Attendance/UpdateShiftManagement',
    method: 'post',
    data: postData
  })
}

// 获取班次详情
export const getShiftDetail = postData => {
  return request({
    url: '/api/services/app/Attendance/GetShiftManagementDetail',
    method: 'post',
    data: { id: postData }
  })
}

// 删除班次
export const submitDeleteShift = postData => {
  return request({
    url: '/api/services/app/Attendance/DeleteShiftManagementByIds',
    method: 'post',
    data: { ids: postData }
  })
}

// =================================================================================================================================

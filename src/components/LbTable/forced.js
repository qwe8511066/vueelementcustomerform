import {getMultistage} from "@/utils/index"
import moment from "moment";
export default {
  selection: {
    renderHeader: (h, { store }) => {
      console.log(store)
      return (
        <el-checkbox
          disabled={store.states.data && store.states.data.length === 0}
          indeterminate={
            store.states.selection.length > 0 && !store.states.isAllSelected
          }
          nativeOn-click={store.toggleAllSelection}
          value={store.states.isAllSelected}
        />
      )
    },
    renderCell: (h, { row, column, store, $index }) => {
      return (
        <el-checkbox
          nativeOn-click={event => event.stopPropagation()}
          value={store.isSelected(row)}
          disabled={
            column.selectable
              ? !column.selectable.call(null, row, $index)
              : false
          }
          on-input={() => {
            store.commit('rowSelectedChanged', row)
          }}
        />
      )
    },
    sortable: false,
    resizable: false
  },
  index: {
    renderHeader: (h, scope) => {
      return <span>{scope.column.label || '#'}</span>
    },
    renderCell: (h, { $index, column }) => {
      let i = $index + 1
      const index = column.index

      if (typeof index === 'number') {
        i = $index + index
      } else if (typeof index === 'function') {
        i = index($index)
      }

      return <div>{i}</div>
    },
    sortable: false
  },
  date:{
    renderHeader: (h, scope) => {
      return <span>{scope.column.label || ''}</span>
    },
    renderCell: (h,scope) => {
      const myColumn = scope._self.column
      const dateType = checkArrayStringObject(myColumn,'dateType')
      const index = dateType?myColumn[dateType.index]:null;
      let value = getMultistage(scope.row,scope.column.property);
          value = value && typeof(value) =='object'?'':value;
      return <div><span>{ value?moment(value).format(index&&index.dateType?index.dateType:'YYYY-MM-DD hh:mm'):'' }</span></div>
    },
    sortable: false
  },
  link:{
    renderHeader: (h, scope) => {
      return <span>{scope.column.label || ''}</span>
    },
    renderCell: (h,scope) => {
      const myColumn = scope._self.column
      const value = myColumn[checkArrayStringObject(myColumn,'linkList').index].linkList 
      console.log(value);
      return ( 
        <div>
        {value.map((item, idx) => (

// onChange={e => {
//   this.setDepPost(scope.row);
//   scope.row.isLeaderPost = !scope.row.isLeaderPost;
//   return false;
// }}

 <el-link onClick={(e)=>{ if(item.url){ window.open(item.url);  } e.stopPropagation()}} type={item.type} underline={item.underline?true:false}>{item.name}</el-link>
))
}
        </div>
        )

    },
    sortable: false
  },
  expand: {
    renderHeader: (h, scope) => {
      return <span>{scope.column.label || ''}</span>
    },
    renderCell: (h, { row, store }, proxy) => {
      const expanded = store.states.expandRows.indexOf(row) > -1
      return (
        <div
          class={
            'el-table__expand-icon ' +
            (expanded ? 'el-table__expand-icon--expanded' : '')
          }
          on-click={e => proxy.handleExpandClick(row, e)}
        >
          <i class='el-icon el-icon-arrow-right' />
        </div>
      )
    },
    sortable: false,
    resizable: false,
    className: 'el-table__expand-column'
  }
}


export function checkArrayStringObject(array, typeString) {
  let type = null;
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      if(array[i][typeString]){
        type = {
          index:i,
          value:array[i][typeString],
        }
        break;
      }
    }
  } else {
    throw new Error('传入的类型错误');
  }
  return type;
}
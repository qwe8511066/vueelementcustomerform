import service from '@/utils/request';

export function UploadFileApi(params, config){
    return service({
        url:'/api/services/app/CommonFile/UploadFileCommon',
        method: 'post',
        data: params,
        config
    })
}
// 单个附件删除：传入参数： delGuids+“，”   //单个附件Guid在后面加个逗号  
// 全部删除：传入参数：delGuids   //附件Guids集合字符串
export function deletFileApi(params, config){
    return service({
        url:'/api/services/app/CommonFile/MoveFileCommon',
        method: 'post',
        config,
        data: params
    })
}

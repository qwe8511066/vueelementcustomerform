﻿import service from '@/utils/request'


/**
 * 当前列表的按钮 权限
 */
export default {
  data() {
    return {
      currentMenusButton: {},
    };
  },
  created() {
    const path = this.$store.getters.application.menusButton[this.$route.path];
    //匹配当前页面的按钮权限
    this.currentMenusButton = path ? path : false
  },
  methods: {
    initButton(h, scope) { 
      const value = this.currentMenusButton;
      // 点击的时候阻止冒泡：e.stopPropagation()
      return value['编辑']||value['查看']||value||['删除']?(
        <span class="publicButtonDivider">
          <el-button type="text" onClick={(e) =>{this.add(1,scope.row);e.stopPropagation()}}value={value}class={value['编辑']?'initial':'node'}icon={value['编辑'] && value['编辑'].value}>编辑</el-button>
          <el-button type="text" onClick={(e)=>{this.add(2,scope.row);e.stopPropagation()}} value={value}class={value['查看']?'initial':'node'}icon={value['查看'] && value['查看'].value}>查看</el-button>
          <el-button type="text" onClick={(e)=>{this.delete(scope.row);e.stopPropagation()}}value={value}class={value['删除']?'initial':'node'}icon={value['删除'] && value['删除'].value} style="color:#f56c6c" >删除</el-button>
        </span>
        ):null
    }
  }
}

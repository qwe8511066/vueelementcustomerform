import {
  checkArray,
  checkArrayString
} from '@/utils/index'
import service from '@/utils/request';
//流程start
import BpmnModeler from 'bpmn-js/lib/Modeler'
import propertiesPanelModule from 'bpmn-js-properties-panel'
import propertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda'
import 'bpmn-js/dist/assets/diagram-js.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'
//流程end
/**
 * 自定义表单的通用方法
 */
export default {
  data() {
    return {
      //优化表单返回值
      validateType: false,

      //路径数据
      routeList: null,
      activeName: '表单内容',
    }
  },

  props: {

  },
  // 自定义表单不能写在create 得等DOM 流程图渲染好了再 初始化数据
  // 自定义表单不能写在create 得等DOM 流程图渲染好了再 初始化数据
  // 自定义表单不能写在create 得等DOM 流程图渲染好了再 初始化数据
  mounted() {
    // 获取到属性ref为“content”的dom节点
    this.container = this.$refs.content
    // 获取到属性ref为“canvas”的dom节点
    const canvas = this.$refs.canvas

    // 建模，官方文档这里讲的很详细
    this.bpmnModeler = new BpmnModeler({
      container: canvas,
      // 添加控制板
      propertiesPanel: {
        parent: '#js-properties-panel'
      }
    })
  },
  methods: {
    //通用自定义表单组件的回调
    outPutForm(value) {
      const dto = value.item.editFrow_TemplateInfo_ColumDto;
      const info = this.data.createtOrUpdateFrow_TemplateInfos[value.index];
      if (dto.controlType == "checkbox") {

        //清空已选或为选的checkbox 
        info.controlSourceDatas.forEach(box => {
          box.value = false;
        });

        //判断已经的数组
        if (checkArray(value.event)) {

          //迭代已选的数组
          value.event.forEach(box => {
            const index = checkArrayString(info.controlSourceDatas, 'label', box)
            if (index != -1) {
              info.controlSourceDatas[index].value = true
            }
          })
        }
      } else if (dto.controlType == 'cityCountyAndDistrict') {
        info.controlSourceDatas = this.getCascaderObj(value.event, this.positionEnum);
      } else if (dto.controlType == 'choicePerson' || dto.controlType == 'choiceStation' || dto.controlType ==
        'choiceDeptartment') {
        info.controlSourceDatas = [];
        value.event.forEach(item => {
          //岗位和部门反选参数是一致的，就写在这里算了
          const label = item.realName ? item.realName : item.name
          info.controlSourceDatas.push({
            value: item.id,
            label: label
          })
        })
      } else if (dto.controlType == 'upload') {} else {
        info.editFrow_TemplateInfo_ColumDto.defaultValue = value.event;
      }
    },

    //市县区获取对象
    getCascaderObj(val, opt) {
      return val.map(function (value, index, array) {
        for (var itm of opt) {
          if (itm.value == value) {
            opt = itm.children;
            return itm;
          }
        }
        return null;
      });
    },

    //获取流程图
    tenantWorkFlow_TemplateInfoBpmnByID(data) {
      return service.post(
        process.env.VUE_APP_FLOW_SERVE_URL +
        `api/services/app/WorkFlowedManager/TenantWorkFlow_TemplateInfoBpmnByID`,
        data
      )
    },

    /**
     * 自定义表单或者业务表单获取详情的接口  改成通用
     */
    initForm(workFlow) {
      this.loading = true;
      const url = this.type === 'true' ? service.get(process.env.VUE_APP_FLOW_SERVE_URL +
        `api/services/app/WorkFlowInstanceManager/GetWorkFlowInstanceFrowTemplateInfoById?workFlow_TemplateInfoId=${this.$route.params.workFlow_TemplateInfoId}&workFlow_InstanceId=${this.$route.params.workFlow_InstanceId}&workFlow_NodeAuditorRecordId=${this.$route.params.workFlow_NodeAuditorRecordId}`
      ) : service.post(process.env.VUE_APP_FLOW_SERVE_URL + `/api/services/app/WorkFlowInstanceManager/Tenant_GetWorkFlowInstanceFrowTemplateInfoById`, workFlow);
      return url;
    },

    //优化已办 待办 的多表单返回值判断
    returnValidate(formUrl) {
      formUrl.validate(v => {
        this.validateType = v;
      });
      return this.validateType
    },

    //获取流程图的里到某个人审批的节点
    tenantWorkFlowTemplateInfoNodeAuditor(data) {
      return service.post(
        process.env.VUE_APP_FLOW_SERVE_URL +
        `api/services/app/WorkFlowInstanceManager/TenantWorkFlowTemplateInfoNodeAuditor`,
        data
      )
    },

    tenantWorkFlow_TemplateInfoPassNodeByID(data) {
      return service.post(
        process.env.VUE_APP_FLOW_SERVE_URL +
        `api/services/app/WorkFlowedManager/TenantWorkFlow_TemplateInfoPassNodeByID`,
        data
      )
    },

    //审批流程记录
    getWorkFlow_NodeRecordAndAuditorRecords() {
      return service.get(
        process.env.VUE_APP_FLOW_SERVE_URL +
        `api/services/app/WorkFlowInstanceManager/GetWorkFlow_NodeRecordAndAuditorRecords?id=` +
        this.$route.params.workFlow_InstanceId
      )
    },

    // 
    //因为3种流程的初始都很类似 就是待办流程传的对象不一致而已

    /**
     * 初始化待办流程  已办流程 我发起的流程详情  
     * 因为3种流程的初始都很类似 就是待办流程传的对象不一致而已
     * @param entityDto 流程图对象
     * @param newEntityDto 流程图节点的对象
     * @param iDDto 流程图节点的审批人对象
     * @param workFlow 获取业务表单的对象
     */
    initHandlingFlow(entityDto, newEntityDto, iDDto, workFlow) {
      Promise.all([
        this.getWorkFlow_NodeRecordAndAuditorRecords(),
        this.tenantWorkFlow_TemplateInfoBpmnByID(entityDto),
        this.tenantWorkFlowTemplateInfoNodeAuditor(newEntityDto),
        this.tenantWorkFlow_TemplateInfoPassNodeByID(iDDto),
        this.initForm(workFlow)
      ]).then(data => {
        this.routeList = data[0]
        let xml = JSON.parse(JSON.stringify(data[1]))
        const win = window
        const x2js = new win.X2JS()
        xml = x2js.xml_str2json(
          win.style_html(
            xml.replace('<?xml version="1.0" encoding="UTF-8"?>', '')
          )
        )
        data[2].forEach(item => {
          const index = checkArrayString(
            xml.definitions.process.task,
            '_id',
            item.nodeId
          )
          if (index != -1) {
            xml.definitions.process.task[index]._name = item.newName
          }
        })
        xml = '<?xml version="1.0" encoding="UTF-8"?>' + x2js.json2xml_str(xml)
        // 将字符串转换成图显示出来
        this.bpmnModeler.importXML(xml, function (err) {
          if (checkArray(data[3])) {
            data[3].forEach(item => {
              //驳回
              if (item.isBack) {
                const g = document.getElementsByTagName('g')
                for (let i = 0; i < g.length; i++) {
                  if (g[i].getAttribute('data-element-id') == id) {
                    g[i].getElementsByTagName('path')[0].style.stroke =
                      v == true ? 'red' : '#000'
                    break
                  }
                }
              }

              if (item.isAudited) {
                let index = -1
                /** 根据已走的节点。改变节点的颜色 */
                const g = document.getElementsByTagName('g')
                for (let i = 0; i < g.length; i++) {
                  if (g[i].getAttribute('data-element-id') == item.nodeId) {
                    index = i
                    break
                  }
                }

                if (
                  item.nodeId.includes('StartEvent') ||
                  item.nodeId.includes('EndEvent')
                ) {
                  //开始的图案或结束的图案
                  g[index]
                    .getElementsByTagName('g')[0]
                    .getElementsByTagName('circle')[0].style.fill =
                    item.isAudited == 1 ? 'C6EDFE' : ''
                } else if (item.nodeId.includes('Task')) {
                  //四边形的图案
                  g[index]
                    .getElementsByTagName('g')[0]
                    .getElementsByTagName('rect')[0].style.fill =
                    item.isAudited == 1 ? '#C6EDFE' : '#FF85FF'
                }
              }
            })
          }
        })
        this.data = data[4]
        this.loading = false
      })
    }

  },
}

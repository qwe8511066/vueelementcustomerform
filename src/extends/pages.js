import service from '@/utils/request'


/**
 * 配置通用的列表页面
 */
export default {
  data() {
    return {
      // 传给后台的分页
      page: {
        // 当前页面
        page: 1,
        // 每页条数
        maxResultCount: 10,
      },

      //列表的头上的表单。用来做筛选数据用的。
      form: {

      },

      //分页的配置
      pagination: {
        total: 0,
        current: 1,
      },

      //数据
      data: [],
      loading: false,

      //列表请求的url 
      listServe: '',


      //删除的url
      deleteServe: '',

      //删除传给后端的key值
      deleteType: 'Id',


      // 详情弹窗
      detailDialog: {
        visible: false,
        title: '',
        id: 0,
        //0新增  1修改  2查看
        type: 0,
      },

    };
  },

  methods: {
    // 重置搜索
    reset() {
      this.page.page = 1;
      if (this.form) {
        Object.keys(this.form).forEach((key) => {
          this.form[key] = undefined
        })
      }
      // this.getList();
    },

    //获取列表数据
    getList(then) {
      console.log('55')
      this.data = []
      this.page = Object.assign({}, this.page, this.form);
      this.loading = true;
      service.post(this.listServe, this.page)
        .then(res => {
          this.loading = false;
          this.data = res.items;
          this.pagination.total = res.totalCount
          if (then) then()
        })
        .catch(err => console.log(err));
    },

    //列表的change事件
    handleTableChange(value) {
      this.page.page = value;
      // this.getList()
    },

    //点击搜索
    search() {
      console.log('444')
      this.page.page = 1;
      this.getList();
    },

    //删除
    deleted(id) {
      this.loading = true;
      const value = {};
      value[this.deleteType] = id
      service
        .post(this.deleteServe, value)
        .then(() => {
          this.$messagSuccess('操作成功');
          this.getList();
        })
        .catch((err) => {
          this.loading = false;
          this.$messagError('操作失败')
        })
    },


    //弹窗的关闭事件  true 刷新列表。false 修改
    close(type) {
      this.detailDialog.visible = false;
      if (type) this.reset();
    }
  },
  // activated() {
  //   this.search();
  // },
}

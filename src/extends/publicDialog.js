/**
 * 配置通用的列表页面
 */
export default {
  data() {
    return {
      loading: false,
    }
  },

  props: {
    // 显示隐藏弹窗
    visible: {
      type: Boolean,
      default: false
    },

    //标题
    title: '',

    // 详情id
    id: "",

    //0新增  1修改  查看
    type: '',
    // 专家库传分类id
    classId: ''
  },

  methods: {
    //关闭弹窗  true刷新父级页面   false 修改父级的visible
    close(type) {
      this.$emit("close", type);

      //关闭弹窗  清空表单
      this.$nextTick(() => {
        if (this.$refs["form"]) this.$refs["form"].resetFields()
      });
    },
  },
}

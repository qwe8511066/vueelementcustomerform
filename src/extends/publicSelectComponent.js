import {
  getProto,
  setFromResult,
  checkArray,
  checkArrayString
} from "@/utils/index"
/**
 * 通用的选择模块  例如：选择用户。选择人 选择部门
 */
export default {
  props: {
    // 类型 单选
    type: {
      type: String,
      default: 'radio'
    },

    showRight: {
      type: Boolean,
      default: true
    },

    showFromButton: {
      type: Boolean,
      default: false
    },

    //判断的id
    checkId: {
      type: String,
      default: 'id'
    }
  },
  data() {
    return {
      currentRow: {
        id: -1
      },
      list: [

      ],
      currentPage: 1,

      span: {
        left: 10,
        right: 14,
      },

      fromButtonSpan: {
        left: 8,
        right: 16,
      }
    };
  },
  created() {
    this.span = this.showRight ? this.span : {
      left: 24,
    }
    this.fromButtonSpan = this.showFromButton ? this.fromButtonSpan : {
      left: 24,
    }
  },
  methods: {
    //点击分页
    handleTableChange(value) {
      this.page.page = value;
      this.getList(() => {
        if (checkArray(this.list)) {
          setTimeout(() => {
            this.list.forEach(item => {
              const index = checkArrayString(this.data, this.checkId, item[this.checkId]);
              this.$refs.table.toggleRowSelection(this.data[index]);
            })
          }, 33)
        }
      });
    },

    //点击行
    rowClick(value) {
      if (value) {
        if (this.type === 'radio') {
          this.currentRow = value ? value : null;
          this.list = [value]
        } else {
          const index = checkArrayString(this.list, this.checkId, value[this.checkId]);
          if (index === -1) {
            this.list.push(value);
          } else {
            this.list.splice(index, 1);
          }
          this.$refs.table.toggleRowSelection(value);
        }
        this.$emit("selectComplete", {
          list: this.list,
          currentRow: this.currentRow
        });
      }
    },

    //点击全选
    selectAll(value) {
      if (checkArray(value)) {
        this.data.forEach(item => {
          const index = checkArrayString(this.list, this.checkId, item[this.checkId]);
          if (index == -1) this.list.push(item)
        })
      } else {
        this.data.forEach(item => {
          const index = checkArrayString(this.list, this.checkId, item[this.checkId]);
          if (index != -1) this.list.splice(index, 1);
        })
      }
      this.$emit("selectComplete", {
        list: this.list,
        currentRow: this.currentRow
      });
    },

    //点击单选
    select(value, row) {
      const index = checkArrayString(this.$refs.table.selection, this.checkId, row[this.checkId]);
      if (index != -1) {
        this.list.push(row)
      } else {
        this.list.splice(index, 1);
      }
      this.$emit("selectComplete", {
        list: this.list,
        currentRow: this.currentRow
      });
    }
  }
}

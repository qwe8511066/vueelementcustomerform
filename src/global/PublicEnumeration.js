//市县区的字典库
import positionEnum from './positionEnum';

const nodeTypeList = [
  { value: 0, label: '提报' },
  { value: 1, label: '审批' },
  { value: 2, label: '审核' },
  { value: 3, label: '承办' },
  { value: 4, label: '阅读' },
]
const customFromTypeSelect = [{
  "label": "输入框",
  "value": "text",
  "dataType": "string"
},
{
  "label": "数字输",
  "value": "inputNumber",
  "dataType": "decimal"
},
{
  "label": "日期框",
  "value": "datePicker",
  "dataType": "date"
}
]
const approverRadioValueEnum = {
  "5": {
    "text": "插入审批人",
    "color": ""
  },
  "6": {
    "text": "会签",
    "color": ""
  },
  "7": {
    "text": "临时审核人",
    "color": ""
  }
}
const approverRadioValueSelect = [{
  "label": "插入审批人",
  "value": 5
},
{
  "label": "会签",
  "value": 6
},

{
  "label": "临时审核人",
  "value": 7
},
]
const applyTypeEnum = {
  "0": {
    "text": "填报",
    "color": ""
  },
  "1": {
    "text": "审批",
    "color": ""
  },
  "2": {
    "text": "审核",
    "color": ""
  },
  "3": {
    "text": "承办",
    "color": ""
  },
  "4": {
    "text": "阅读",
    "color": ""
  },
  "5": {
    "text": "添加审批人",
    "color": ""
  },
  "6": {
    "text": "会签",
    "color": ""
  },
  "7": {
    "text": "添加审核人",
    "color": ""
  }
}

const WorkFlowedStateSelect = [{
  "label": "全部",
  "value": ""
},
{
  "label": "处理中",
  "value": "0"
},

{
  "label": "撤销",
  "value": 2
},
{
  "label": "已完成",
  "value": 3
},

{
  "label": "已驳回",
  "value": 5
}
]
export default {
  customFromTypeSelect,
  applyTypeEnum,
  WorkFlowedStateSelect,
  approverRadioValueEnum,
  approverRadioValueSelect,
  positionEnum,
  nodeTypeList
}

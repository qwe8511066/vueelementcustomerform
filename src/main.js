import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
import './assets/font/iconfont.css';
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import moment from 'moment'
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log
import ElTreeSelect from 'el-tree-select';
import * as filters from './filters' // global filters
import publicPages from '@/components/publicPages'
import {
  closeCurTag
} from './utils/index'
import PublicEnumeration from '@/global/PublicEnumeration'

//拖拽表单
import dragula from 'dragula'
Vue.use(dragula);


//打印
import Print from 'vue-print-nb'
Vue.use(Print);

Vue.prototype.$closeCurTag = closeCurTag // 关闭当前标签页面
Vue.prototype.$PublicEnumeration = PublicEnumeration //全局枚举库

// 定义 Loading 对象
const PublicPages = {
  // install 是默认的方法。当外界在 use 这个组件的时候，就会调用本身的 install 方法，同时传一个 Vue 这个类的参数。
  install: function (Vue) {
    Vue.component('public-pages', publicPages)
  }
}

//全局应用select树型组件
Vue.use(ElTreeSelect);

//全局引用 二次封装的table组件 三次封装成自己的table 组件
Vue.use(PublicPages);
Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})
Vue.use(moment)

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

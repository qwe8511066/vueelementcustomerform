
import Layout from '@/layout'

const flowRouter = {
  path: '/flowEdit',
  component: Layout,
  redirect: '/flowEdit',
  name: '流程设计',
  meta: {
    title: '流程设计',
  },
  children: [
    {
      path: '/flowEdit',
      component: () => import('@/views/flow/flowEdit'),
      meta: {
        title: '流程设计',
      },
    },
  ]
}

export default flowRouter



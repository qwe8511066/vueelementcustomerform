import Layout from '@/layout'

const formRouter = {
  path: '/formModule',
  component: Layout,
  redirect: '/formModule',
  name: '自定义表单',
  meta: {
    title: '自定义表单',
  },
  children: [{
    path: '/formModule',
    component: () => import('@/views/form/customForm'),
    meta: {
      title: '自定义表单',
    },
  }
]
}

export default formRouter

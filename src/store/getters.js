const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  userInfo: state => state.user.userInfo,
  avatar: state => state.user.userInfo.avatar,
  realname: state => state.user.userInfo.realname,
  username: state => state.user.userInfo.username,
  roleIds: state => state.user.userInfo.roleIds,
  organizationName: state => state.user.userInfo.organizationName,
  organizationId: state => state.user.userInfo.organizationId,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
}
export default getters


import {
  checkMenusButton,
  checkMenu,
  getProto,
} from "@/utils/index";
import service from '@/utils/request'
/**
 * 当前应用的参数
 */
const state = {
  // 菜单
  currentModule: [],

  // 应用id 
  applicationId: '1',

  /**
   * 菜单的按钮权限  
   * 对象格式。去除根菜单  格式:{'url':{'新增':'icon'}}
   **/
  menusButton: null,

  //应用标题
  applicationTitle:'',
}

const mutations = {
  SET_ERROR_CURRENTMODULE: (state, currentModule) => {
    state.currentModule = currentModule
  },
  SET_APPLICATIONID: (state, applicationId) => {
    state.applicationId = applicationId
  },
  SET_MENUSBUTTON: (state, menus) => {
    state.menusButton = menus
  },
  SET_APPLICATIONTITLE: (state, applicationTitle) => {
    state.applicationTitle = applicationTitle
  },
}

const actions = {
  setCurrentModule({
    commit
  }, currentModule) {
    commit('SET_ERROR_CURRENTMODULE', currentModule)
  },
  setApplicationId({
    commit
  }, applicationId) {
    commit('SET_APPLICATIONID', applicationId)
  },
  setMenusButton({
    commit
  }, menus) {
    commit('SET_MENUSBUTTON', menus)
  },
  setApplicationTitle({
    commit
  }, applicationTitle) {
    commit('SET_APPLICATIONTITLE', applicationTitle)
  },

  authGetMenuTree({ commit }, applicationId) {
    return new Promise((resolve, reject) => {
      try {
        service.post('api/services/app/Menu/AuthGetMenuTree', {
          id: applicationId
        }).then(data => {
          commit('SET_APPLICATIONID', applicationId)
          commit('SET_ERROR_CURRENTMODULE', checkMenu(data.authTree))
          commit('SET_MENUSBUTTON', checkMenusButton(data.authTree))
          commit('SET_APPLICATIONTITLE',data.applicationName)
          resolve(data.authTree)
        }).catch(error => {
          reject(error)
        })
      } catch (error) {
      }
      
  })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

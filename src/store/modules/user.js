// import { login, logout, getInfo } from '@/api/user'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'
import router, {
  resetRouter
} from '@/router'
/* eslint-disable */
const state = {
  token: getToken(),
  userInfo: {
    // 真实姓名
    realname: null,
    // 用户id
    id: null,
    /**
     * 角色id
     *
     * 权限配置有误： -1
     * 系统管理员(默认为住建厅管理员):  1
     * 住建厅人员:  2 ——————管理员 2-1，普通用户 2-2
     * 其它政府单位人员: 3 ——————管理员 3-1，普通用户 3-2
     * 社会企业: 4 ——————管理员 4-1，普通用户 4-2
     * 社会群众: 5
     */
    roleIds: [],
    // 部门名称
    organizationName: null,
    // 部门id
    organizationsId: null,
    // 账户名
    username: null,
    // 头像
    avatar: null,
  }
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_INFO: (state, userInfo) => {
    state.userInfo = userInfo
  },
  SET_ROLE_IDS: (state, roleIds) => {
    state.userInfo.roleIds = roleIds
  },
  SET_REALNAME: (state, realname) => {
    state.userInfo.realname = realname
  },
  SET_USERNAME: (state, username) => {
    state.userInfo.username = username
  },
  SET_AVATAR: (state, avatar) => {
    state.userInfo.avatar = avatar
  }
}

const actions = {
  // user login
  login({
    commit
  }, userInfo) {
    const {
      username,
      password
    } = userInfo
    return new Promise((resolve, reject) => {
      /* 测试数据-start */
      if (username === 'admin') {
        const token = 'test'

        commit('SET_TOKEN', token)
        setToken(token)
        resolve(token)
      } else {
        reject();
      }
      /* 测试数据-end */
    })
  },

  // get user info
  getInfo({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      /* 测试数据-start */
      const user = {
        realname: '管理员',
        roleIds: ['1'],
        id: 666,
        organizationName: null,
        organizationsId: null,
        username: 'admin',
        avatar: null
      }
      commit('SET_USER_INFO', user)
      resolve(user)
      /* 测试数据-end */
    })
  },

  // user logout
  logout({
    commit,
    state,
    dispatch
  }) {
    return new Promise((resolve, reject) => {
      /* 测试数据-start */
      commit('SET_TOKEN', '')
      commit('SET_USER_INFO', {
        realname: null,
        roleIds: [],
        id: null,
        organizationName: null,
        organizationsId: null,
        username: null,
        avatar: null
      })

      removeToken()
      resetRouter()
      resolve()
      /* 测试数据-emd */
    })
  },

  // remove token
  resetToken({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_USER_INFO', {
        realname: null,
        roleIds: [],
        id: null,
        organizationName: null,
        organizationsId: null,
        username: null,
        avatar: null
      })
      removeToken()
      resolve()
    })
  },

  // dynamically modify permissions
  changeRoles({
    commit,
    dispatch
  }, role) {
    return new Promise(async resolve => {
      const token = role + '-token'

      commit('SET_TOKEN', token)
      setToken(token)

      const {
        roles
      } = await dispatch('getInfo')

      resetRouter()

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, {
        root: true
      })

      // dynamically add accessible routes
      router.addRoutes(accessRoutes)

      // reset visited views and cached views
      dispatch('tagsView/delAllViews', null, {
        root: true
      })

      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

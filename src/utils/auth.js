import Cookies from 'js-cookie'

//不要乱动这个字段  因为连着后续系统的token
//不要乱动这个字段  因为连着后续系统的token
//不要乱动这个字段  因为连着后续系统的token
const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token, options = {}) {
  return Cookies.set(TokenKey, token, options)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function myGetToken(name) {
  return Cookies.get(name)
}

export function mySetToken(name, token) {
  return Cookies.set(name, token)
}

export function myRemoveToken(name) {
  return Cookies.remove(name)
}

import defaultSettings from '@/settings'


//由于是动态应用+动态菜单。不需要写这个
const title = defaultSettings.title || 'Vue Element Admin'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
